﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObvyHomeLibrary.BookDetailsService.Contract.Configuration
{
    public class GoogleBookApiConfiguration
    {
        public string ApiUrlFormat { get; set; }

        public string ApiKey { get; set; }
    }
}
