﻿namespace ObvyHomeLibrary.BookDetailsService.Contract.Dto
{
    public class BookDetails
    {
        public string Isbn10 { get; set; }

        public string Isbn13 { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Genre { get; set; }

        public BookDetails(string isbn10, string isbn13, string title, string author, string genre)
        {
            Isbn10 = isbn10;
            Isbn13 = isbn13;
            Title = title;
            Author = author;
            Genre = genre;
        }
    }
}
