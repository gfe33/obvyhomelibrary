﻿using System.Threading.Tasks;
using ObvyHomeLibrary.BookDetailsService.Contract.Dto;

namespace ObvyHomeLibrary.BookDetailsService.Contract.Proxy
{
    public interface IBookDataProxy
    {
        Task<BookDetails> GetBookDataAsync(string isbnCode);
    }
}
