﻿using System.Threading.Tasks;
using ObvyHomeLibrary.BookDetailsService.Contract.Dto;

namespace ObvyHomeLibrary.BookDetailsService.Contract.Service
{
    public interface IBookDetailsService
    {
        Task<BookDetails> GetBookDetailsAsync(string isbnCode);
    }
}
