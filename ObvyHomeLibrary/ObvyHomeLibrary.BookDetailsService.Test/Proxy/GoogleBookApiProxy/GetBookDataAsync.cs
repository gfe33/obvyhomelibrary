﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ObvyHomeLibrary.BookDetailsService.Contract.Configuration;
using ObvyHomeLibrary.BookDetailsService.Contract.Dto;

namespace ObvyHomeLibrary.BookDetailsService.Test.Proxy.GoogleBookApiProxy
{
    [TestClass]
    public class GetBookDataAsync
    {
        private static BookDetailsService.Proxy.GoogleBookApiProxy _proxy;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            GoogleBookApiConfiguration config = new GoogleBookApiConfiguration
            {
                ApiUrlFormat = "https://www.googleapis.com/books/v1/volumes?key={0}&q=isbn:{1}",
                ApiKey = "AIzaSyDqWtCe6JC2zWnlWbwNJglFc-ih_VeQt6I"
            };

            _proxy = new BookDetailsService.Proxy.GoogleBookApiProxy(config);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
            _proxy.Dispose();
        }

        [TestMethod, TestCategory("Integration")]
        public async Task GoogleBookApiProxy_GetBookDataAsync_WithGoodIsbnCode_ReturnsBookDetails()
        {
            BookDetails bookDetails = await _proxy.GetBookDataAsync("0451526538");

            Assert.IsNotNull(bookDetails);
            Assert.AreEqual("0451526538", bookDetails.Isbn10);
            Assert.AreEqual("9780451526533", bookDetails.Isbn13);
            Assert.AreEqual("The Adventures of Tom Sawyer", bookDetails.Title);
            Assert.AreEqual("Mark Twain", bookDetails.Author);
            Assert.AreEqual("Fiction", bookDetails.Genre);
        }

        [TestMethod, TestCategory("Integration")]
        public async Task GoogleBookApiProxy_GetBookDataAsync_WithWrongIsbn_ReturnsNull()
        {
            BookDetails bookDetails = await _proxy.GetBookDataAsync("1234567890");

            Assert.IsNull(bookDetails);
        }

        [TestMethod, TestCategory("Integration")]
        public async Task GoogleBookApiProxy_GetBookDataAsync_WithoutIsbnCode_ThrowsException()
        {
            BookDetails bookDetails = null;
            Exception ex = null;

            try
            {
                bookDetails = await _proxy.GetBookDataAsync(null);
            }
            catch (Exception e)
            {
                ex = e;
            }

            Assert.IsNull(bookDetails);
            Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            Assert.AreEqual("isbnCode", (ex as ArgumentNullException)?.ParamName);
        }
    }
}
