﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using NSubstitute.ExceptionExtensions;
using ObvyHomeLibrary.BookDetailsService.Contract.Dto;
using ObvyHomeLibrary.BookDetailsService.Contract.Proxy;

namespace ObvyHomeLibrary.BookDetailsService.Test.Service.BookDetailsService
{
    [TestClass]
    public class GetBookDetailsAsync
    {
        private static IBookDataProxy _bookDataProxyMock;
        private static ObvyHomeLibrary.BookDetailsService.Service.BookDetailsService _service;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _bookDataProxyMock = Substitute.For<IBookDataProxy>();
            _bookDataProxyMock.GetBookDataAsync(Arg.Is<string>(s => s != null))
                .Returns(new BookDetails("1234567890", "1234567890123", "titleTest", "authorTest", "genreTest"));
            _bookDataProxyMock.GetBookDataAsync(Arg.Is<string>(s => s == null))
                .Throws(new ArgumentNullException("isbnCode"));

            _service = new ObvyHomeLibrary.BookDetailsService.Service.BookDetailsService(_bookDataProxyMock);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _bookDataProxyMock.ClearReceivedCalls();
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookDetailsService_GetBookDetailsAsync_WithIsbnCode_ReturnsBookDetails()
        {
            BookDetails bookDetails = await _service.GetBookDetailsAsync("1234567890123");

            await _bookDataProxyMock.Received(1).GetBookDataAsync(Arg.Is("1234567890123"));
            Assert.IsInstanceOfType(bookDetails, typeof(BookDetails));
            Assert.AreEqual("1234567890", bookDetails.Isbn10);
            Assert.AreEqual("1234567890123", bookDetails.Isbn13);
            Assert.AreEqual("titleTest", bookDetails.Title);
            Assert.AreEqual("authorTest", bookDetails.Author);
            Assert.AreEqual("genreTest", bookDetails.Genre);
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookDetailsService_GetBookDetailsAsync_WithBadIsbnCode_ThrowsException()
        {
            Exception ex = null;
            BookDetails bookDetails = null;

            try
            {
                bookDetails = await _service.GetBookDetailsAsync("123456789012");
            }
            catch (Exception e)
            {
                ex = e;
            }

            await _bookDataProxyMock.Received(0).GetBookDataAsync(Arg.Any<string>());
            Assert.IsNull(bookDetails);
            Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            Assert.AreEqual("isbnCode", (ex as ArgumentException)?.ParamName);
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookDetailsService_GetBookDetailsAsync_WithoutIsbnCode_ThrowsException()
        {
            Exception ex = null;
            BookDetails bookDetails = null;

            try
            {
                bookDetails = await _service.GetBookDetailsAsync(null);
            }
            catch (Exception e)
            {
                ex = e;
            }

            await _bookDataProxyMock.Received(0).GetBookDataAsync(Arg.Any<string>());
            Assert.IsNull(bookDetails);
            Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            Assert.AreEqual("isbnCode", (ex as ArgumentNullException)?.ParamName);
        }

    }
}