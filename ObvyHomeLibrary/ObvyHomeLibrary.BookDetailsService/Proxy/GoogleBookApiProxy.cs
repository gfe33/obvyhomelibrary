﻿using System;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using ObvyHomeLibrary.BookDetailsService.Contract.Configuration;
using ObvyHomeLibrary.BookDetailsService.Contract.Proxy;
using ObvyHomeLibrary.BookDetailsService.Contract.Dto;

namespace ObvyHomeLibrary.BookDetailsService.Proxy
{
    public class GoogleBookApiProxy : IBookDataProxy, IDisposable
    {
        private readonly HttpClient _httpClient;
        private readonly GoogleBookApiConfiguration _configuration;

        public GoogleBookApiProxy(GoogleBookApiConfiguration configuration)
        {
            _httpClient = new HttpClient();
            _configuration = configuration;
        }

        public void Dispose()
        {
            _httpClient.Dispose();
        }

        public async Task<BookDetails> GetBookDataAsync(string isbnCode)
        {
            if (isbnCode == null)
                throw new ArgumentNullException(nameof(isbnCode));

            string apiUrlFormat = _configuration.ApiUrlFormat;
            string apiKey = _configuration.ApiKey;
            string apiUrl = string.Format(apiUrlFormat, apiKey, isbnCode);

            string apiResult;
            using (HttpResponseMessage response = (await _httpClient.GetAsync(apiUrl)).EnsureSuccessStatusCode())
            {
                using (HttpContent content = response.Content)
                {
                    apiResult = await content.ReadAsStringAsync();
                }
            }

            JObject apiResultJObject = JObject.Parse(apiResult);
            JToken firstResult = apiResultJObject["items"]?.FirstOrDefault();

            if (firstResult == null)
                return null;

            JToken volumeInfo = firstResult["volumeInfo"];
            JToken[] isbnCodeBlocks = volumeInfo["industryIdentifiers"].ToArray();
            string isbn10 = isbnCodeBlocks
                .SingleOrDefault(icb => icb["type"].Value<string>() == "ISBN_10")
                ?["identifier"].Value<string>();
            string isbn13 = isbnCodeBlocks
                .SingleOrDefault(icb => icb["type"].Value<string>() == "ISBN_13")
                ?["identifier"].Value<string>();
            string title = volumeInfo["title"]?.Value<string>();
            string author = volumeInfo["authors"]?.FirstOrDefault()?.Value<string>();
            string genre = volumeInfo["categories"]?.FirstOrDefault()?.Value<string>();

            return new BookDetails(isbn10, isbn13, title, author, genre);
        }
    }
}
