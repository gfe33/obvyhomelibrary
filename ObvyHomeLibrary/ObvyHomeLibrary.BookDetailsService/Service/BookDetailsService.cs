﻿using System;
using System.Threading.Tasks;
using ObvyHomeLibrary.BookDetailsService.Contract.Proxy;
using ObvyHomeLibrary.BookDetailsService.Contract.Dto;
using ObvyHomeLibrary.BookDetailsService.Contract.Service;
using System.Text.RegularExpressions;

namespace ObvyHomeLibrary.BookDetailsService.Service
{
    public class BookDetailsService : IBookDetailsService
    {
        private readonly IBookDataProxy _bookDataProxy;

        public BookDetailsService(IBookDataProxy bookDataProxy)
        {
            _bookDataProxy = bookDataProxy;
        }

        public Task<BookDetails> GetBookDetailsAsync(string isbnCode)
        {
            if (isbnCode == null)
                throw new ArgumentNullException(nameof(isbnCode));

            Regex isbnRegex = new Regex(@"^(\d{13}|\d{10})$");
            if (!isbnRegex.IsMatch(isbnCode))
                throw new ArgumentException("The ISBN must be 10 or 13 digits.", nameof(isbnCode));

            return _bookDataProxy.GetBookDataAsync(isbnCode);
        }
    }
}
