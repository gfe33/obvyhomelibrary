﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ObvyHomeLibrary.BookStoreService.Contract.Dto
{
    public class BookSearchQuery
    {
        public string Isbn10 { get; set; }

        public string Isbn13 { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Genre { get; set; }
    }
}
