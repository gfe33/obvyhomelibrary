﻿using ObvyHomeLibrary.BookStoreService.Contract.Model;
using System.Collections.Generic;

namespace ObvyHomeLibrary.BookStoreService.Contract.Dto
{
    public class BookSearchResult
    {
        public IReadOnlyCollection<Book> Books { get; set; }
    }
}
