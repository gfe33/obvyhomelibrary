﻿namespace ObvyHomeLibrary.BookStoreService.Contract.Exception
{
    public class BookAlreadyAddedException : System.Exception
    {
        private const string MessageFormat = "The book with ISBN10 {0} and ISBN13 {1} already exists in the store.";

        public string Isbn10 { get; }

        public string Isbn13 { get; }

        public BookAlreadyAddedException(string isbn10, string isbn13)
            : base(string.Format(MessageFormat, isbn10, isbn13))
        {
            Isbn10 = isbn10;
            Isbn13 = isbn13;
        }
    }
}
