﻿namespace ObvyHomeLibrary.BookStoreService.Contract.Exception
{
    public class BookIdNotFoundException : System.Exception
    {
        private const string MessageFormat = "The book with ID {0} does not exist.";

        public long Id { get; }

        public BookIdNotFoundException(long id)
            : base(string.Format(MessageFormat, id))
        {
            Id = id;
        }
    }
}
