﻿using System;

namespace ObvyHomeLibrary.BookStoreService.Contract.Model
{
    public class Book : BaseEntity
    {
        public string Isbn10 { get; set; }

        public string Isbn13 { get; set; }

        public string Title { get; set; }
        
        public string Author { get; set; }
        
        public string Genre { get; set; }
        
        public bool IsBorrowed { get; set; }
        
        public DateTime? BorrowDateTime { get; set; }
        
        public string BorrowName { get; set; }
        
        public string BorrowComment { get; set; }
    }
}
