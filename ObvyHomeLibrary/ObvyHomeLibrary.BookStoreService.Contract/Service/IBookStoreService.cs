﻿using System.Threading.Tasks;
using ObvyHomeLibrary.BookStoreService.Contract.Dto;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.BookStoreService.Contract.Service
{
    public interface IBookStoreService
    {
        Task AddBookToStoreAsync(Book book);

        Task<Book> GetBookAsync(long id);

        Task<BookSearchResult> SearchBooksAsync(BookSearchQuery query);

        Task LendBookAsync(long id, string name, string comment);

        Task RecoverBookAsync(long id);
    }
}
