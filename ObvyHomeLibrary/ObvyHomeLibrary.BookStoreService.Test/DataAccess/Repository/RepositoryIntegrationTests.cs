﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ObvyHomeLibrary.BookStoreService.Contract.Model;
using ObvyHomeLibrary.BookStoreService.DataAccess;

namespace ObvyHomeLibrary.BookStoreService.Test.DataAccess.Repository
{
    [TestClass]
    public class RepositoryIntegrationTests
    {
        private static Repository<Book> _repo;
        private Book _testBook1;
        private Book _testBook2;


        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            var dbContextOptions = new DbContextOptionsBuilder<BookContext>()
                .UseInMemoryDatabase("ObvyHomeLibrary").Options;

            var bookContext = new BookContext(dbContextOptions);

            _repo = new Repository<Book>(bookContext);
        }

        [TestInitialize]
        public async Task TestInitialize()
        {
            _testBook1 = new Book
            {
                Isbn10 = "1234567890",
                Isbn13 = "1234567890123",
                Title = "TestTitle1",
                Author = "TestAuthor1",
                Genre = "TestGenre1",
                IsBorrowed = true,
                BorrowName = "TestBorrowName1",
                BorrowDateTime = new DateTime(2018, 05, 16, 12, 30, 00),
                BorrowComment = "TestBorrowComment1"
            };
            _testBook2 = new Book
            {
                Isbn10 = "2345678901",
                Isbn13 = "2345678901234",
                Title = "TestTitle2",
                Author = "TestAuthor2",
                Genre = "TestGenre2",
                IsBorrowed = true,
                BorrowName = "TestBorrowName2",
                BorrowDateTime = new DateTime(2018, 05, 16, 12, 40, 00),
                BorrowComment = "TestBorrowComment2"
            };

            await _repo.InsertAsync(_testBook1);
            await _repo.InsertAsync(_testBook2);
        }

        [TestCleanup]
        public async Task TestCleanup()
        {
            await _repo.DeleteAsync(_testBook1);
            await _repo.DeleteAsync(_testBook2);
        }

        [TestMethod, TestCategory("Integration")]
        public async Task Repository_AnyAsync_WithTruePredicate_ReturnsTrue()
        {
            bool result = await _repo.AnyAsync(b => b.Isbn13 == "1234567890123");
            Assert.IsTrue(result);
        }

        [TestMethod, TestCategory("Integration")]
        public async Task Repository_AnyAsync_WithFalsePredicate_ReturnsFalse()
        {
            bool result = await _repo.AnyAsync(b => b.Isbn10 == "098765432123");
            Assert.IsFalse(result);
        }

        [TestMethod, TestCategory("Integration")]
        public async Task Repository_AnyAsync_WithoutPredicate_ThrowsException()
        {
            bool result = false;
            Exception ex = null;

            try
            {
                result = await _repo.AnyAsync(null);
            }
            catch (Exception e)
            {
                ex = e;
            }

            Assert.AreEqual(false, result);
            Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            Assert.AreEqual("predicate", (ex as ArgumentNullException)?.ParamName);
        }

        [TestMethod, TestCategory("Integration")]
        public async Task Repository_GetAsync_WithGoodId_ReturnsEntity()
        {
            Book result = await _repo.GetAsync(_testBook1.Id);

            Assert.IsNotNull(result);
            Assert.AreEqual(_testBook1.Id, result.Id);
            Assert.AreEqual(_testBook1.Isbn10, result.Isbn10);
            Assert.AreEqual(_testBook1.Isbn13, result.Isbn13);
            Assert.AreEqual(_testBook1.Title, result.Title);
            Assert.AreEqual(_testBook1.Author, result.Author);
            Assert.AreEqual(_testBook1.Genre, result.Genre);
            Assert.AreEqual(_testBook1.IsBorrowed, result.IsBorrowed);
            Assert.AreEqual(_testBook1.BorrowName, result.BorrowName);
            Assert.AreEqual(_testBook1.BorrowDateTime, result.BorrowDateTime);
            Assert.AreEqual(_testBook1.BorrowComment, result.BorrowComment);
        }

        [TestMethod, TestCategory("Integration")]
        public async Task Repository_GetAsync_WithBadId_ReturnsNull()
        {
            Book result = await _repo.GetAsync(-1);

            Assert.IsNull(result);
        }

        [TestMethod, TestCategory("Integration")]
        public async Task Repository_GetAllAsync_ReturnsAllEntities()
        {
            IOrderedEnumerable<Book> result = (await _repo.GetAllAsync()).OrderBy(b => b.Id);

            Assert.AreEqual(2, result.Count(), "Result count");

            Book resultBook1 = result.ElementAtOrDefault(0);
            Book resultBook2 = result.ElementAtOrDefault(1);

            Assert.IsNotNull(resultBook1, "Result 1");
            Assert.AreEqual(_testBook1.Id, resultBook1.Id, "Id 1");
            Assert.AreEqual(_testBook1.Isbn10, resultBook1.Isbn10, "Isbn10 1");
            Assert.AreEqual(_testBook1.Isbn13, resultBook1.Isbn13, "Isbn13 1");
            Assert.AreEqual(_testBook1.Title, resultBook1.Title, "Title 1");
            Assert.AreEqual(_testBook1.Author, resultBook1.Author, "Author 1");
            Assert.AreEqual(_testBook1.Genre, resultBook1.Genre, "Genre 1");
            Assert.AreEqual(_testBook1.IsBorrowed, resultBook1.IsBorrowed, "IsBorrowed 1");
            Assert.AreEqual(_testBook1.BorrowName, resultBook1.BorrowName, "BorrowName 1");
            Assert.AreEqual(_testBook1.BorrowDateTime, resultBook1.BorrowDateTime, "BorrowDateTime 1");
            Assert.AreEqual(_testBook1.BorrowComment, resultBook1.BorrowComment, "BorrowComment 1");

            Assert.IsNotNull(resultBook2, "Result 2");
            Assert.AreEqual(_testBook2.Id, resultBook2.Id, "Id 2");
            Assert.AreEqual(_testBook2.Isbn10, resultBook2.Isbn10, "Isbn10 2");
            Assert.AreEqual(_testBook2.Isbn13, resultBook2.Isbn13, "Isbn13 2");
            Assert.AreEqual(_testBook2.Title, resultBook2.Title, "Title 2");
            Assert.AreEqual(_testBook2.Author, resultBook2.Author, "Author 2");
            Assert.AreEqual(_testBook2.Genre, resultBook2.Genre, "Genre 2");
            Assert.AreEqual(_testBook2.IsBorrowed, resultBook2.IsBorrowed, "IsBorrowed 2");
            Assert.AreEqual(_testBook2.BorrowName, resultBook2.BorrowName, "BorrowName 2");
            Assert.AreEqual(_testBook2.BorrowDateTime, resultBook2.BorrowDateTime, "BorrowDateTime 2");
            Assert.AreEqual(_testBook2.BorrowComment, resultBook2.BorrowComment, "BorrowComment 2");
        }
    }
}
