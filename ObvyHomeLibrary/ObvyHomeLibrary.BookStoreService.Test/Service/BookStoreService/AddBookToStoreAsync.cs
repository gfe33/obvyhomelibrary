﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using ObvyHomeLibrary.BookStoreService.Contract.DataAccess;
using ObvyHomeLibrary.BookStoreService.Contract.Exception;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.BookStoreService.Test.Service.BookStoreService
{
    [TestClass]
    public class AddBookToStoreAsync
    {
        private static IRepository<Book> _bookRepositoryMock;
        private static ObvyHomeLibrary.BookStoreService.Service.BookStoreService _service;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _bookRepositoryMock = Substitute.For<IRepository<Book>>();
            _bookRepositoryMock.InsertAsync(null).ReturnsForAnyArgs(Task.Delay(50));

            _service = new ObvyHomeLibrary.BookStoreService.Service.BookStoreService(_bookRepositoryMock);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _bookRepositoryMock.ClearReceivedCalls();
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_AddBookToStoreAsync_WithNewBook_InsertsBook()
        {
            _bookRepositoryMock.AnyAsync(null).ReturnsForAnyArgs(Task.FromResult(false));

            Book testBook = new Book
            {
                Isbn10 = "1234567890",
                Isbn13 = "1234567890123",
                Title = "TestTitle",
                Author = "TestAuthor",
                Genre = "TestGenre",
                IsBorrowed = true,
                BorrowName = "TestBorrowName",
                BorrowDateTime = new DateTime(2018, 05, 16, 12, 30, 00),
                BorrowComment = "TestBorrowComment"
            };

            await _service.AddBookToStoreAsync(testBook);

            await _bookRepositoryMock.ReceivedWithAnyArgs(1).AnyAsync(null);
            await _bookRepositoryMock.Received(1).InsertAsync(Arg.Is(testBook));
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_AddBookToStoreAsync_WithExistingBook_ThrowsException()
        {
            _bookRepositoryMock.AnyAsync(null).ReturnsForAnyArgs(Task.FromResult(true));
            Exception ex = null;

            Book testBook = new Book
            {
                Isbn10 = "1234567890",
                Isbn13 = "1234567890123",
                Title = "TestTitle",
                Author = "TestAuthor",
                Genre = "TestGenre",
                IsBorrowed = true,
                BorrowName = "TestBorrowName",
                BorrowDateTime = new DateTime(2018, 05, 16, 12, 30, 00),
                BorrowComment = "TestBorrowComment"
            };

            try
            {
                await _service.AddBookToStoreAsync(testBook);
            }
            catch (Exception e)
            {
                ex = e;
            }

            await _bookRepositoryMock.ReceivedWithAnyArgs(1).AnyAsync(null);
            await _bookRepositoryMock.ReceivedWithAnyArgs(0).InsertAsync(null);
            Assert.IsInstanceOfType(ex, typeof(BookAlreadyAddedException));
            BookAlreadyAddedException bookAkreadyAddedEx = ex as BookAlreadyAddedException;
            Assert.AreEqual("1234567890", bookAkreadyAddedEx.Isbn10);
            Assert.AreEqual("1234567890123", bookAkreadyAddedEx.Isbn13);
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_AddBookToStoreAsync_WithBookWithoutIsbnCode_ThrowsException()
        {
            Exception ex = null;

            Book testBook = new Book
            {
                Isbn10 = "",
                Isbn13 = "",
                Title = "TestTitle",
                Author = "TestAuthor",
                Genre = "TestGenre",
                IsBorrowed = true,
                BorrowName = "TestBorrowName",
                BorrowDateTime = new DateTime(2018, 05, 16, 12, 30, 00),
                BorrowComment = "TestBorrowComment"
            };

            try
            {
                await _service.AddBookToStoreAsync(testBook);
            }
            catch (Exception e)
            {
                ex = e;
            }

            await _bookRepositoryMock.ReceivedWithAnyArgs(0).AnyAsync(null);
            await _bookRepositoryMock.ReceivedWithAnyArgs(0).InsertAsync(null);
            Assert.IsInstanceOfType(ex, typeof(ArgumentException));
            ArgumentException argumentEx = ex as ArgumentException;
            Assert.IsTrue(argumentEx.ParamName.Contains("Isbn10"), "ISBN 10");
            Assert.IsTrue(argumentEx.ParamName.Contains("Isbn13"), "ISBN 13");
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_AddBookToStoreAsync_WithoutBook_ThrowsException()
        {
            Exception ex = null;

            try
            {
                await _service.AddBookToStoreAsync(null);
            }
            catch (Exception e)
            {
                ex = e;
            }

            await _bookRepositoryMock.ReceivedWithAnyArgs(0).AnyAsync(null);
            await _bookRepositoryMock.ReceivedWithAnyArgs(0).InsertAsync(null);
            Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            Assert.AreEqual("book", (ex as ArgumentNullException)?.ParamName);
        }
    }
}
