﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using ObvyHomeLibrary.BookStoreService.Contract.DataAccess;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.BookStoreService.Test.Service.BookStoreService
{
    [TestClass]
    public class GetBookAsync
    {
        private static IRepository<Book> _bookRepositoryMock;
        private static ObvyHomeLibrary.BookStoreService.Service.BookStoreService _service;
        private static Book _testBook;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _testBook = new Book
            {
                Isbn10 = "1234567890",
                Isbn13 = "1234567890123",
                Title = "TestTitle",
                Author = "TestAuthor",
                Genre = "TestGenre",
                IsBorrowed = true,
                BorrowName = "TestBorrowName",
                BorrowDateTime = new DateTime(2018, 05, 16, 12, 30, 00),
                BorrowComment = "TestBorrowComment"
            };

            _bookRepositoryMock = Substitute.For<IRepository<Book>>();
            _bookRepositoryMock.GetAsync(0).ReturnsForAnyArgs(_testBook);

            _service = new ObvyHomeLibrary.BookStoreService.Service.BookStoreService(_bookRepositoryMock);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _bookRepositoryMock.ClearReceivedCalls();
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_AddBookToStoreAsync_WithNewBook_InsertsBook()
        {
            _bookRepositoryMock.AnyAsync(null).ReturnsForAnyArgs(Task.FromResult(false));

            Book book = await _service.GetBookAsync(5);

            await _bookRepositoryMock.ReceivedWithAnyArgs(1).GetAsync(5);
            Assert.AreEqual(_testBook, book);
        }
    }
}
