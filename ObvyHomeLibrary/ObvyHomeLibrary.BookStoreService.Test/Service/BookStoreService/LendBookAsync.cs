﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using ObvyHomeLibrary.BookStoreService.Contract.DataAccess;
using ObvyHomeLibrary.BookStoreService.Contract.Exception;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.BookStoreService.Test.Service.BookStoreService
{
    [TestClass]
    public class LendBookAsync
    {
        private static IRepository<Book> _bookRepositoryMock;
        private static ObvyHomeLibrary.BookStoreService.Service.BookStoreService _service;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _bookRepositoryMock = Substitute.For<IRepository<Book>>();
            _bookRepositoryMock.UpdateAsync(null).ReturnsForAnyArgs(Task.Delay(50));

            _service = new ObvyHomeLibrary.BookStoreService.Service.BookStoreService(_bookRepositoryMock);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _bookRepositoryMock.ClearReceivedCalls();
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_LendBookAsync_WithGoodBookId_UpdatesBook()
        {
            Book testBook = new Book
            {
                Isbn10 = "1234567890",
                Isbn13 = "1234567890123",
                IsBorrowed = false,
                BorrowName = null,
                BorrowDateTime = null,
                BorrowComment = null
            };
            _bookRepositoryMock.GetAsync(Arg.Any<long>()).Returns(testBook);

            DateTime timeBefore = DateTime.Now;
            await _service.LendBookAsync(1, "New Borrow Name", "New Borrow Comment");
            DateTime timeAfter = DateTime.Now;

            await _bookRepositoryMock.Received(1).GetAsync(1);
            await _bookRepositoryMock.Received(1).UpdateAsync(Arg.Is(testBook));
            Assert.IsTrue(testBook.IsBorrowed);
            Assert.AreEqual("New Borrow Name", testBook.BorrowName);
            Assert.IsTrue(timeBefore <= testBook.BorrowDateTime);
            Assert.IsTrue(timeAfter >= testBook.BorrowDateTime);
            Assert.AreEqual("New Borrow Comment", testBook.BorrowComment);
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_LendBookAsync_WithBorrowedBookId_ThrowsException()
        {
            Book testBook = new Book
            {
                Isbn10 = "1234567890",
                Isbn13 = "1234567890123",
                IsBorrowed = true,
                BorrowName = "Old Borrow Name",
                BorrowDateTime = new DateTime(2018, 10, 16, 16, 00, 00),
                BorrowComment = "Old Borrow Comment"
            };
            _bookRepositoryMock.GetAsync(Arg.Any<long>()).Returns(testBook);
            Exception ex = null;

            try
            {
                await _service.LendBookAsync(1, "New Borrow Name", "New Borrow Comment");
            }
            catch (Exception e)
            {
                ex = e;
            }

            await _bookRepositoryMock.Received(1).GetAsync(1);
            await _bookRepositoryMock.Received(0).UpdateAsync(Arg.Any<Book>());
            Assert.IsInstanceOfType(ex, typeof(BookAlreadyBorrowedException));
            var bookAlreadyBorrowedEx = ex as BookAlreadyBorrowedException;
            Assert.AreEqual("1234567890", bookAlreadyBorrowedEx.Isbn10);
            Assert.AreEqual("1234567890123", bookAlreadyBorrowedEx.Isbn13);
            Assert.IsTrue(testBook.IsBorrowed);
            Assert.AreEqual("Old Borrow Name", testBook.BorrowName);
            Assert.AreEqual(new DateTime(2018, 10, 16, 16, 00, 00), testBook.BorrowDateTime);
            Assert.AreEqual("Old Borrow Comment", testBook.BorrowComment);
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_LendBookAsync_WithBadBookId_ThrowsException()
        {
            _bookRepositoryMock.GetAsync(Arg.Any<long>()).Returns(null as Book);
            Exception ex = null;

            try
            {
                await _service.LendBookAsync(1, "BorrowName", "BorrowComment");
            }
            catch (Exception e)
            {
                ex = e;
            }

            await _bookRepositoryMock.Received(1).GetAsync(1);
            await _bookRepositoryMock.Received(0).UpdateAsync(Arg.Any<Book>());
            Assert.IsInstanceOfType(ex, typeof(BookIdNotFoundException));
            Assert.AreEqual(1, (ex as BookIdNotFoundException)?.Id);
        }
    }
}
