﻿using System;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using ObvyHomeLibrary.BookStoreService.Contract.DataAccess;
using ObvyHomeLibrary.BookStoreService.Contract.Exception;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.BookStoreService.Test.Service.BookStoreService
{
    [TestClass]
    public class RecoverBookAsync
    {
        private static IRepository<Book> _bookRepositoryMock;
        private static ObvyHomeLibrary.BookStoreService.Service.BookStoreService _service;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _bookRepositoryMock = Substitute.For<IRepository<Book>>();

            _service = new ObvyHomeLibrary.BookStoreService.Service.BookStoreService(_bookRepositoryMock);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _bookRepositoryMock.ClearReceivedCalls();
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_RecoverBookAsync_WithGoodBookId_UpdatesBook()
        {
            Book testBook = new Book
            {
                Isbn10 = "1234567890",
                Isbn13 = "1234567890123",
                IsBorrowed = true,
                BorrowName = "Old Borrow Name",
                BorrowDateTime = new DateTime(2018, 10, 16, 16, 00, 00),
                BorrowComment = "Old Borrow Comment"
            };
            _bookRepositoryMock.GetAsync(Arg.Any<long>()).Returns(testBook);
            
            await _service.RecoverBookAsync(1);

            await _bookRepositoryMock.Received(1).GetAsync(1);
            await _bookRepositoryMock.Received(1).UpdateAsync(Arg.Is(testBook));
            Assert.IsFalse(testBook.IsBorrowed);
            Assert.IsNull(testBook.BorrowName);
            Assert.IsNull(testBook.BorrowDateTime);
            Assert.IsNull(testBook.BorrowComment);
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_RecoverBookAsync_WithNotBorrowedBookId_ThrowsException()
        {
            Book testBook = new Book
            {
                Isbn10 = "1234567890",
                Isbn13 = "1234567890123",
                IsBorrowed = false,
                BorrowName = null,
                BorrowDateTime = null,
                BorrowComment = null
            };
            _bookRepositoryMock.GetAsync(Arg.Any<long>()).Returns(testBook);
            Exception ex = null;

            try
            {
                await _service.RecoverBookAsync(1);
            }
            catch (Exception e)
            {
                ex = e;
            }

            await _bookRepositoryMock.Received(1).GetAsync(1);
            await _bookRepositoryMock.Received(0).UpdateAsync(Arg.Any<Book>());
            Assert.IsInstanceOfType(ex, typeof(BookNotBorrowedException));
            BookNotBorrowedException bookNotBorrowedEx = ex as BookNotBorrowedException;
            Assert.AreEqual("1234567890", bookNotBorrowedEx.Isbn10);
            Assert.AreEqual("1234567890123", bookNotBorrowedEx.Isbn13);
            Assert.IsFalse(testBook.IsBorrowed);
            Assert.IsNull(testBook.BorrowName);
            Assert.IsNull(testBook.BorrowDateTime);
            Assert.IsNull(testBook.BorrowComment);
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_RecoverBookAsync_WithBadBookId_ThrowsException()
        {
            _bookRepositoryMock.GetAsync(Arg.Any<long>()).Returns(null as Book);
            Exception ex = null;

            try
            {
                await _service.RecoverBookAsync(1);
            }
            catch (Exception e)
            {
                ex = e;
            }

            await _bookRepositoryMock.Received(1).GetAsync(1);
            await _bookRepositoryMock.Received(0).UpdateAsync(Arg.Any<Book>());
            Assert.IsInstanceOfType(ex, typeof(BookIdNotFoundException));
            Assert.AreEqual(1, (ex as BookIdNotFoundException)?.Id);
        }
    }
}
