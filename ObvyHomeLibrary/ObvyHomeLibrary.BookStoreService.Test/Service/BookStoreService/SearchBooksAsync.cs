﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;
using ObvyHomeLibrary.BookStoreService.Contract.DataAccess;
using ObvyHomeLibrary.BookStoreService.Contract.Dto;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.BookStoreService.Test.Service.BookStoreService
{
    [TestClass]
    public class SearchBooksAsync
    {
        private static IRepository<Book> _bookRepositoryMock;
        private static ObvyHomeLibrary.BookStoreService.Service.BookStoreService _service;
        private static Book _book1, _book2, _book3;

        [ClassInitialize]
        public static void ClassInitialize(TestContext context)
        {
            _book1 = new Book
            {
                Isbn10 = "0000011111",
                Isbn13 = "0000011111222",
                Title = "Test Title 0 1",
                Author = "Test Author 0 1",
                Genre = "Test Genre 0 1"
            };
            _book2 = new Book
            {
                Isbn10 = "1111122222",
                Isbn13 = "1111122222333",
                Title = "Test Title 1 2",
                Author = "Test Author 1 2",
                Genre = "Test Genre 1 2"
            };
            _book3 = new Book
            {
                Isbn10 = "2222233333",
                Isbn13 = "2222233333444",
                Title = "Test Title 1 2 3",
                Author = "Test Author 1 2 3",
                Genre = "Test Genre 1 2 3"
            };

            _bookRepositoryMock = Substitute.For<IRepository<Book>>();
            _bookRepositoryMock.GetAllAsync().Returns(new[] {_book1, _book2, _book3});

            _service = new ObvyHomeLibrary.BookStoreService.Service.BookStoreService(_bookRepositoryMock);
        }

        [TestInitialize]
        public void TestInitialize()
        {
            _bookRepositoryMock.ClearReceivedCalls();
        }
        
        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_SearchBooksAsync_WithoutQuery_ThrowsException()
        {
            Exception ex = null;

            try
            {
                await _service.SearchBooksAsync(null);
            }
            catch (Exception e)
            {
                ex = e;
            }

            Assert.IsInstanceOfType(ex, typeof(ArgumentNullException));
            Assert.AreEqual("query", (ex as ArgumentNullException)?.ParamName);
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_SearchBooksAsync_WithQueryIsbn10_ReturnsBooks()
        {
            BookSearchQuery query = new BookSearchQuery {Isbn10 = "11111"};

            BookSearchResult result = await _service.SearchBooksAsync(query);

            await _bookRepositoryMock.Received(1).GetAllAsync();
            Assert.AreEqual(2, result.Books.Count);
            Assert.IsTrue(result.Books.Contains(_book1));
            Assert.IsTrue(result.Books.Contains(_book2));
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_SearchBooksAsync_WithQueryIsbn13_ReturnsBooks()
        {
            BookSearchQuery query = new BookSearchQuery { Isbn13 = "222" };

            BookSearchResult result = await _service.SearchBooksAsync(query);

            await _bookRepositoryMock.Received(1).GetAllAsync();
            Assert.AreEqual(3, result.Books.Count);
            Assert.IsTrue(result.Books.Contains(_book1));
            Assert.IsTrue(result.Books.Contains(_book2));
            Assert.IsTrue(result.Books.Contains(_book3));
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_SearchBooksAsync_WithQueryTitle_ReturnsBooks()
        {
            BookSearchQuery query = new BookSearchQuery { Title = "2" };

            BookSearchResult result = await _service.SearchBooksAsync(query);

            await _bookRepositoryMock.Received(1).GetAllAsync();
            Assert.AreEqual(2, result.Books.Count);
            Assert.IsTrue(result.Books.Contains(_book2));
            Assert.IsTrue(result.Books.Contains(_book3));
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_SearchBooksAsync_WithQueryAuthor_ReturnsBooks()
        {
            BookSearchQuery query = new BookSearchQuery { Author = "0" };

            BookSearchResult result = await _service.SearchBooksAsync(query);

            await _bookRepositoryMock.Received(1).GetAllAsync();
            Assert.AreEqual(1, result.Books.Count);
            Assert.IsTrue(result.Books.Contains(_book1));
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_SearchBooksAsync_WithQueryGenre_ReturnsBooks()
        {
            BookSearchQuery query = new BookSearchQuery { Genre = "3" };

            BookSearchResult result = await _service.SearchBooksAsync(query);

            await _bookRepositoryMock.Received(1).GetAllAsync();
            Assert.AreEqual(1, result.Books.Count);
            Assert.IsTrue(result.Books.Contains(_book3));
        }

        [TestMethod, TestCategory("Unit")]
        public async Task BookStoreService_SearchBooksAsync_WithQueryAllParameters_ReturnsBooks()
        {
            BookSearchQuery query = new BookSearchQuery
            {
                Isbn10 = "11111",
                Isbn13 = "11111",
                Title = "1",
                Author = "2",
                Genre = "1"
            };

            BookSearchResult result = await _service.SearchBooksAsync(query);

            await _bookRepositoryMock.Received(1).GetAllAsync();
            Assert.AreEqual(1, result.Books.Count);
            Assert.IsTrue(result.Books.Contains(_book2));
        }
    }
}
