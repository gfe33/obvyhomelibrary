﻿using Microsoft.EntityFrameworkCore;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.BookStoreService.DataAccess
{
    public class BookContext : DbContext
    {
        public BookContext(DbContextOptions<BookContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            new BookMap(modelBuilder.Entity<Book>());
        }
    }
}
