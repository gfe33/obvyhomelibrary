﻿using Microsoft.EntityFrameworkCore.Metadata.Builders;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.BookStoreService.DataAccess
{
    public class BookMap
    {
        public BookMap(EntityTypeBuilder<Book> entityBuilder)
        {
            entityBuilder.HasKey(b => b.Id);
            entityBuilder.Property(b => b.Isbn10);
            entityBuilder.Property(b => b.Isbn13);
            entityBuilder.Property(b => b.Title);
            entityBuilder.Property(b => b.Author);
            entityBuilder.Property(b => b.Genre);
            entityBuilder.Property(b => b.IsBorrowed).IsRequired();
            entityBuilder.Property(b => b.BorrowName);
            entityBuilder.Property(b => b.BorrowDateTime);
            entityBuilder.Property(b => b.BorrowComment);
        }
    }
}
