﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ObvyHomeLibrary.BookStoreService.Contract.DataAccess;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.BookStoreService.DataAccess
{
    public class Repository<TEntity> : IRepository<TEntity>
        where TEntity : BaseEntity
    {
        private readonly DbContext _context;
        private readonly DbSet<TEntity> _entities;

        public Repository(DbContext context)
        {
            _context = context;
            _entities = context.Set<TEntity>();
        }

        public Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            if (predicate == null)
                throw new ArgumentNullException(nameof(predicate));

            return _entities.AnyAsync(predicate);
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
            => await _entities.ToArrayAsync();

        public Task<TEntity> GetAsync(long id)
            => _entities.FindAsync(id);
        
        public Task InsertAsync(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            return _entities.AddAsync(entity).ContinueWith(e => _context.SaveChangesAsync());
        }

        public Task UpdateAsync(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            _context.Update(entity);
            return _context.SaveChangesAsync();
        }

        public Task DeleteAsync(TEntity entity)
        {
            if (entity == null)
                throw new ArgumentNullException(nameof(entity));

            _entities.Remove(entity);
            return _context.SaveChangesAsync();
        }
    }
}
