﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ObvyHomeLibrary.BookStoreService.Contract.DataAccess;
using ObvyHomeLibrary.BookStoreService.Contract.Dto;
using ObvyHomeLibrary.BookStoreService.Contract.Exception;
using ObvyHomeLibrary.BookStoreService.Contract.Model;
using ObvyHomeLibrary.BookStoreService.Contract.Service;

namespace ObvyHomeLibrary.BookStoreService.Service
{
    public class BookStoreService : IBookStoreService
    {
        private readonly IRepository<Book> _bookRepository;

        public BookStoreService(IRepository<Book> bookRepository)
        {
            _bookRepository = bookRepository;
        }

        public async Task AddBookToStoreAsync(Book book)
        {
            if (book == null)
                throw new ArgumentNullException(nameof(book));

            if (string.IsNullOrWhiteSpace(book.Isbn10) && string.IsNullOrWhiteSpace(book.Isbn13))
                throw new ArgumentException("The ISBN code must not be null or white space.", string.Join(", ", nameof(book.Isbn10), nameof(book.Isbn13)));

            if (await _bookRepository.AnyAsync(b => b.Isbn10 == book.Isbn10 || b.Isbn13 == book.Isbn13))
                throw new BookAlreadyAddedException(book.Isbn10, book.Isbn13);

            await _bookRepository.InsertAsync(book);
        }

        public Task<Book> GetBookAsync(long id)
        {
            return _bookRepository.GetAsync(id);
        }

        public async Task<BookSearchResult> SearchBooksAsync(BookSearchQuery query)
        {
            if (query == null)
                throw new ArgumentNullException(nameof(query));

            List<Book> books = (await _bookRepository.GetAllAsync())
                .Where(b => IsBookMatchingQuery(b, query))
                .ToList();

            return new BookSearchResult {Books = books.AsReadOnly()};
        }

        public async Task LendBookAsync(long id, string name, string comment)
        {
            Book book = await _bookRepository.GetAsync(id);

            if (book == null)
                throw new BookIdNotFoundException(id);

            if (book.IsBorrowed)
                throw new BookAlreadyBorrowedException(book.Isbn10, book.Isbn13);

            book.IsBorrowed = true;
            book.BorrowName = name;
            book.BorrowComment = comment;
            book.BorrowDateTime = DateTime.Now;

            await _bookRepository.UpdateAsync(book);
        }

        public async Task RecoverBookAsync(long id)
        {
            if (id < 0)
                throw new ArgumentException("The ID must be greater than 0.", nameof(id));

            Book book = await _bookRepository.GetAsync(id);


            if (book == null)
                throw new BookIdNotFoundException(id);

            if (!book.IsBorrowed)
                throw new BookNotBorrowedException(book.Isbn10, book.Isbn13);

            book.IsBorrowed = false;
            book.BorrowName = null;
            book.BorrowComment = null;
            book.BorrowDateTime = null;

            await _bookRepository.UpdateAsync(book);
        }

        private bool IsBookMatchingQuery(Book book, BookSearchQuery query)
        {
            bool match = true;
            match &= string.IsNullOrEmpty(query.Isbn10) || book.Isbn10.Contains(query.Isbn10);
            match &= string.IsNullOrEmpty(query.Isbn13) || book.Isbn13.Contains(query.Isbn13);
            match &= string.IsNullOrEmpty(query.Title) || book.Title.Contains(query.Title);
            match &= string.IsNullOrEmpty(query.Author) || book.Author.Contains(query.Author);
            match &= string.IsNullOrEmpty(query.Genre) || book.Genre.Contains(query.Genre);
            return match;
        }
    }
}
