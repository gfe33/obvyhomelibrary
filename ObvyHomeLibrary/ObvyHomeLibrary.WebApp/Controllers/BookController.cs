﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using ObvyHomeLibrary.BookDetailsService.Contract.Dto;
using ObvyHomeLibrary.BookDetailsService.Contract.Service;
using ObvyHomeLibrary.BookStoreService.Contract.Dto;
using ObvyHomeLibrary.BookStoreService.Contract.Model;
using ObvyHomeLibrary.BookStoreService.Contract.Service;
using ObvyHomeLibrary.WebApp.Dto;
using ObvyHomeLibrary.WebApp.Models;

namespace ObvyHomeLibrary.WebApp.Controllers
{
    public class BookController : Controller
    {
        private const string LastSearchedBookDetailsSessionKey = "LastSearchedBookDetails";

        private readonly IBookDetailsService _bookDetailsService;
        private readonly IBookStoreService _bookStoreService;

        public BookController(IBookDetailsService bookDetailsService, IBookStoreService bookStoreService)
        {
            _bookDetailsService = bookDetailsService;
            _bookStoreService = bookStoreService;
        }

        [HttpGet]
        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetData(string isbnCode)
        {
            try
            {
                Regex isbnRegex = new Regex(@"\D");
                string filteredIsbnCode = isbnRegex.Replace(isbnCode, string.Empty);

                BookDetails bookDetails = await _bookDetailsService.GetBookDetailsAsync(filteredIsbnCode);

                if (bookDetails == null)
                {
                    return Json(null);
                }

                await HttpContext.Session.LoadAsync();
                string bookDetailsJson = JsonConvert.SerializeObject(bookDetails);
                HttpContext.Session.SetString(LastSearchedBookDetailsSessionKey, bookDetailsJson);
                await HttpContext.Session.CommitAsync();

                BookDto bookDto = new BookDto(bookDetails);
                return Json(bookDto);
            }

            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Add()
        {
            try
            {
                await HttpContext.Session.LoadAsync();
                string bookDetailsJson = HttpContext.Session.GetString(LastSearchedBookDetailsSessionKey);
                if (string.IsNullOrEmpty(bookDetailsJson))
                {
                    throw new Exception($"Impossible to find the book to add.");
                }

                BookDetails bookDetails = JsonConvert.DeserializeObject<BookDetails>(bookDetailsJson);
                Book book = new Book
                {
                    Isbn10 = bookDetails.Isbn10,
                    Isbn13 = bookDetails.Isbn13,
                    Title = bookDetails.Title,
                    Author = bookDetails.Author,
                    Genre = bookDetails.Genre
                };
                await _bookStoreService.AddBookToStoreAsync(book);

            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
            return Json(null);
        }

        [HttpPost]
        public async Task<IActionResult> Search(BookSearchQuery searchQuery)
        {
            try
            {
                BookSearchResult searchResult = await _bookStoreService.SearchBooksAsync(searchQuery);
                IEnumerable<BookDto> bookDtoList = searchResult?.Books?.Select(b => new BookDto(b));
                return Json(bookDtoList);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpGet]
        public async Task<IActionResult> Details(long id)
        {
            try
            {
                Book book = await _bookStoreService.GetBookAsync(id);
                BookDto bookDto = book != null ? new BookDto(book) : null;
                return Json(bookDto);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Lend(LendBookDto lendBookDto)
        {
            try
            {
                await _bookStoreService.LendBookAsync(lendBookDto.Id, lendBookDto.BorrowName, lendBookDto.BorrowComment);
                return Json(null);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }

        [HttpPost]
        public async Task<IActionResult> Recover(long id)
        {
            try
            {
                await _bookStoreService.RecoverBookAsync(id);
                return Json(null);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);
            }
        }
    }
}