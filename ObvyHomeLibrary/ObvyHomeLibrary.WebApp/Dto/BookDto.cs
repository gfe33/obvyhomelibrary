﻿using System;
using ObvyHomeLibrary.BookDetailsService.Contract.Dto;
using ObvyHomeLibrary.BookStoreService.Contract.Model;

namespace ObvyHomeLibrary.WebApp.Models
{
    public class BookDto
    {
        public long Id { get; set; }

        public string Isbn10 { get; set; }

        public string Isbn13 { get; set; }

        public string Title { get; set; }

        public string Author { get; set; }

        public string Genre { get; set; }

        public bool IsBorrowed { get; set; }

        public DateTime? BorrowDateTime { get; set; }

        public string BorrowName { get; set; }

        public string BorrowComment { get; set; }

        public BookDto()
        {

        }

        public BookDto(Book book)
        {
            Id = book.Id;
            Isbn10 = book.Isbn10;
            Isbn13 = book.Isbn13;
            Title = book.Title;
            Author = book.Author;
            Genre = book.Genre;
            IsBorrowed = book.IsBorrowed;
            BorrowDateTime = book.BorrowDateTime;
            BorrowName = book.BorrowName;
            BorrowComment = book.BorrowComment;
        }

        public BookDto(BookDetails bookDetails)
        {
            Isbn10 = bookDetails.Isbn10;
            Isbn13 = bookDetails.Isbn13;
            Title = bookDetails.Title;
            Author = bookDetails.Author;
            Genre = bookDetails.Genre;
        }
    }
}
