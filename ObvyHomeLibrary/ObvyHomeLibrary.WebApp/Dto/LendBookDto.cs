﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ObvyHomeLibrary.WebApp.Dto
{
    public class LendBookDto
    {
        public long Id { get; set; }

        public string BorrowName { get; set; }

        public string BorrowComment { get; set; }
    }
}
