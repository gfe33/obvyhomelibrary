﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using ObvyHomeLibrary.BookDetailsService.Contract.Configuration;
using ObvyHomeLibrary.BookDetailsService.Contract.Proxy;
using ObvyHomeLibrary.BookDetailsService.Contract.Service;
using ObvyHomeLibrary.BookDetailsService.Proxy;
using ObvyHomeLibrary.BookStoreService.Contract.DataAccess;
using ObvyHomeLibrary.BookStoreService.Contract.Model;
using ObvyHomeLibrary.BookStoreService.Contract.Service;
using ObvyHomeLibrary.BookStoreService.DataAccess;
using System;
using System.Reflection;

namespace ObvyHomeLibrary.WebApp
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddSession();

            services.AddTransient<IBookDetailsService, BookDetailsService.Service.BookDetailsService>();

            services.AddSingleton<IBookDataProxy, GoogleBookApiProxy>();

            GoogleBookApiConfiguration googleBookApiConfig = new GoogleBookApiConfiguration();
            Configuration.GetSection("GoogleBookApiConfiguration").Bind(googleBookApiConfig);
            services.AddSingleton(googleBookApiConfig);

            services.AddTransient<IBookStoreService, BookStoreService.Service.BookStoreService>();
            services.AddTransient<IRepository<Book>, Repository<Book>>();

            string usedConnectionString = Configuration.GetValue<string>("UseConnectionString");
            DbContextOptionsBuilder<BookContext> optionsBuilder = new DbContextOptionsBuilder<BookContext>();
            Action<DbContextOptionsBuilder> optionsAction;
            switch (usedConnectionString)
            {
                case "sqlite":
                    string assemblyName = GetType().Assembly.FullName;
                    optionsAction = opt => opt.UseSqlite(
                        Configuration.GetConnectionString(usedConnectionString), 
                        sqlite => sqlite.MigrationsAssembly(assemblyName));
                    break;
                case "inmemory":
                    optionsAction = opt => opt.UseInMemoryDatabase(Configuration.GetConnectionString(usedConnectionString));
                    break;
                default:
                    throw new Exception("The configuration value UseConnectionString is invalid. Possible values : sqlite, inmemory.");
            }
            optionsAction.Invoke(optionsBuilder);
            services.AddDbContext<BookContext>(optionsAction);
            services.AddTransient<DbContext, BookContext>(s => new BookContext(optionsBuilder.Options));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSession();

            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Book/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Book}/{action=Index}/{id?}");
            });

            app.ApplicationServices.GetService<DbContext>().Database.Migrate();
        }
    }
}
