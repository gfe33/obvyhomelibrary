﻿function nullIfEmpty(str) {
    if (str.length === 0) {
        return null;
    }
    return str;
}

function filterNotNumChar(event) {
    event.target.value = event.target.value.replace(/\D/g, "");
}

function action(msg) {
    var messageTime = new Date().toLocaleTimeString();
    var messageLine = "<tr class='action'><td>" + messageTime + "</td><td>" + msg + "</td></tr>";
    $("#messagesHistoryBody").prepend(messageLine);
}

function success(msg) {
    var messageTime = new Date().toLocaleTimeString();
    var messageLine = "<tr class='message'><td>" + messageTime + "</td><td>" + msg + "</td></tr>";
    $("#messagesHistoryBody").prepend(messageLine);
}

function warning(msg) {
    var messageTime = new Date().toLocaleTimeString();
    var messageLine = "<tr class='warning'><td>" + messageTime + "</td><td>" + msg + "</td></tr>";
    $("#messagesHistoryBody").prepend(messageLine);
}

function error(msg) {
    var messageTime = new Date().toLocaleTimeString();
    var messageLine = "<tr class='error'><td>" + messageTime + "</td><td>" + msg + "</td></tr>";
    $("#messagesHistoryBody").prepend(messageLine);
}

function handleError(jqXhr, textStatus, errorThrown) {
    error(jqXhr.responseText);
}

function getData() {
    var isbnCode = $("#searchDataIsbnCodeTextbox");
    var isbnCodeVal = isbnCode.val();
    action("Searching the book with ISBN " + isbnCodeVal + "...");
    if (isbnCodeVal.length === 0) {
        error("Please provide a ISBN to search.");
        return;
    }
    $.ajax({
        url: "/Book/GetData",
        method: "POST",
        data: { isbnCode: isbnCodeVal },
        cache: true,
        success: function (data) {
            var searchDataResult = $("#searchDataResult");
            if (data) {
                $("#dataIsbn10").text(data.isbn10);
                $("#dataIsbn13").text(data.isbn13);
                $("#dataTitle").text(data.title);
                $("#dataAuthor").text(data.author);
                $("#dataGenre").text(data.genre);
                searchDataResult.show();
                success("Book with ISBN 10" + data.isbn10 + " and ISBN 13 " + data.isbn13 + " found.");
            } else {
                searchDataResult.hide();
                warning("Book with ISBN " + isbnCodeVal + " not found.");
            }
        },
        error: handleError
    });
}

function add() {
    var isbn10 = $("#dataIsbn10").text();
    var isbn13 = $("#dataIsbn13").text();
    action("Adding the book with ISBN 10" + isbn10 + " and ISBN 13 " + isbn13 + "to the store...");
    $.ajax({
        url: "/Book/Add",
        method: "POST",
        cache: false,
        success: function () {
            $("#searchDataIsbnCodeTextbox").val("");
            $("#searchDataResult").hide();
            success("Book with ISBN 10" + isbn10 + " and ISBN 13 " + isbn13 + " added to the store.");
        },
        error: handleError
    });
}

function search() {
    var searchData = {
        isbn10: nullIfEmpty($("#searchIsbn10Textbox").val()),
        isbn13: nullIfEmpty($("#searchIsbn13Textbox").val()),
        title: nullIfEmpty($("#searchTitleTextbox").val()),
        author: nullIfEmpty($("#searchAuthorTextbox").val()),
        genre: nullIfEmpty($("#searchGenreTextbox").val())
    };
    action("Searching the books in the store (parameters : " + searchData.isbn10 + ", " +
        searchData.isbn13 + ", " + searchData.title + ", " + searchData.author + ", " + searchData.genre + ")...");
    $.ajax({
        url: "/Book/Search",
        method: "POST",
        data: searchData,
        cache: false,
        success: function (data) {
            var resultsTable = $("#searchResultsTable");
            var resultsBody = resultsTable.find("tbody");
            resultsBody.children().remove();
            if (data.length > 0) {
                for (var i in data) {
                    var row = "<tr><td><span>" +
                        data[i].isbn10 +
                        "</span></td><td><span>" +
                        data[i].isbn13 +
                        "</span></td><td><span>" +
                        data[i].title +
                        "</span></td><td><span>" +
                        data[i].author +
                        "</span></td><td><span>" +
                        data[i].genre +
                        "</span></td><td><a href='#details' onclick='showDetails(" +
                        data[i].id + ", " + data[i].isbn10 + ", " + data[i].isbn13 +
                        ");'>Details</a></td>";
                    resultsBody.append(row);
                }
                resultsTable.show();
                success("Found " + data.length + " books in the store.");
            } else {
                resultsTable.hide();
                warning("No book found in the store.");
            }
        },
        error: handleError
    });
}

function showDetails(id, isbn10, isbn13) {
    action("Showing details for the book with ISBN 10" + isbn10 + " and ISBN 13 " + isbn13 + "...");
    $.ajax({
        url: "/Book/Details/" + id.toString(),
        method: "GET",
        cache: false,
        success: function (data) {
            if (data) {
                $("#detailsIdHidden").val(data.id);
                $("#detailsIsbnCode").text(data.isbnCode);
                $("#detailsTitle").text(data.title);
                $("#detailsAuthor").text(data.author);
                $("#detailsGenre").text(data.genre);
                if (data.isBorrowed) {
                    $("#detailsIsNotBorrowedTable").hide();
                    $("#detailsIsBorrowedTable").show();
                    $("#detailsBorrowName").text(data.borrowName);
                    var borrowDateTime = new Date(data.borrowDateTime);
                    $("#detailsBorrowDate").text(borrowDateTime.toLocaleDateString());
                    $("#detailsBorrowTime").text(borrowDateTime.toLocaleTimeString());
                    $("#detailsBorrowComment").text(data.borrowComment);
                } else {
                    $("#detailsIsBorrowedTable").hide();
                    $("#detailsIsNotBorrowedTable").show();
                    $("#detailsBorrowNameTextBox").val("");
                    $("#detailsBorrowCommentTextBox").val("");
                }
                $("#details").show();
                success("Details for the book with ISBN 10" + data.isbn10 + " and ISBN 13 " + data.isbn13 + " shown.");
            } else {
                $("#details").hide();
                error("Error getting the details for the book with ISBN 10" + data.isbn10 + " and ISBN 13 " + data.isbn13 + ".");
            }
        },
        error: handleError
    });
}

function lend() {
    var lendData = {
        id: $("#detailsIdHidden").val(),
        borrowName: $("#detailsBorrowNameTextbox").val(),
        borrowComment: $("#detailsBorrowCommentTextbox").val()
    };
    var isbn10 = $("#detailsIsbn10").text();
    var isbn13 = $("#detailsIsbn13").text();
    action("Lending the book with ISBN 10" + isbn10 + " and ISBN 13 " + isbn13 +
        " to " + lendData.borrowName + "(Comment : " + lendData.borrowComment + ")...");
    $.ajax({
        url: "/Book/Lend",
        method: "POST",
        data: lendData,
        cache: false,
        success: function () {
            success("Book with ISBN 10" + isbn10 + " and ISBN 13 " + isbn13 + " lent.");
            showDetails(bookId);
        },
        error: handleError
    });
}

function recover() {
    var bookId = $("#detailsIdHidden").val();
    var isbn10 = $("#detailsIsbn10").text();
    var isbn13 = $("#detailsIsbn13").text();
    action("Recovering the book with ISBN 10" + isbn10 + " and ISBN 13 " + isbn13 + "...");
    $.ajax({
        url: "/Book/Recover",
        method: "POST",
        data: { id: bookId },
        cache: false,
        success: function () {
            success("Book with ISBN 10" + isbn10 + " and ISBN 13 " + isbn13 + " recovered.");
            showDetails(bookId);
        },
        error: handleError
    });
}